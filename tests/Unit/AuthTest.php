<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;

class AuthTest extends TestCase
{
    /**
     * A login unit test
     *
     * @return void
     */
    public function testLogin()
    {
        $response = $this->json('POST', '/api/v1/auth/login', ['email'=>'admin@local.com', 'password'=>'admin123']);

        $response->assertStatus(200)
                 ->assertJson(['status'=>'success']);
    }
    

    public function testLogout()
    {        
        $response = $this->json('POST', '/api/v1/auth/login', ['email'=>'admin@local.com', 'password'=>'admin123']);
        $token    = $response->headers->get('Authorization');

        $headers = ['Authorization'=>'Bearer ' . $token];
        
        $this->json('POST', '/api/v1/auth/logout', [], $headers)
             ->assertStatus(200)
             ->assertJson(['status'=>'success']);             
    }
}
