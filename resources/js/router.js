import VueRouter from 'vue-router'
// Pages
import Home from './pages/Home'
import Login from './pages/Login'
import BookIndex from './pages/book/BookIndex'
import BookCreate from './pages/book/BookCreate'
import BookEdit from './pages/book/BookEdit'
import BookFormWrap from './pages/book/BookFormWrap'

// Routes
const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      auth: undefined
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      auth: false
    }
  },
  {
    path: '/book',
    name: 'book',
    component: BookIndex,
    meta: {
      auth: true
    }
  },
  {
    path: '/book/create',
    name: 'bookCreate',
    component: BookFormWrap,
    meta: {
      auth: true
    }
  },
  {
    path: '/book/edit/:id',
    name: 'bookEdit',
    component: BookFormWrap,
    meta: {
      auth: true
    }
  },
]
const router = new VueRouter({
  history: true,
  mode: 'history',
  routes,
})
export default router