<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title'=> $faker->sentence(5),
        'category_id'=>rand(1, 5),
        'description'=>''
    ];
});
