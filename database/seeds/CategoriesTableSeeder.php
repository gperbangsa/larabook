<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name'=>'History',
            'description'=>''
        ]);

        DB::table('categories')->insert([
            'name'=>'Technology',
            'description'=>''
        ]);

        DB::table('categories')->insert([
            'name'=>'Art',
            'description'=>''
        ]);

        DB::table('categories')->insert([
            'name'=>'Algebra',
            'description'=>''
        ]);

        DB::table('categories')->insert([
            'name'=>'Literature',
            'description'=>''
        ]);
    }
}
