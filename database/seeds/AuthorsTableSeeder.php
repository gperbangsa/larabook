<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
            'name'=>'JK Rowling',
            'description'=>''
        ]);

        DB::table('authors')->insert([
            'name'=>'Chairil Anwar',
            'description'=>''
        ]);

        DB::table('authors')->insert([
            'name'=>'Pidi Baiq',
            'description'=>''
        ]);

        DB::table('authors')->insert([
            'name'=>'Stephen King',
            'description'=>''
        ]);

        DB::table('authors')->insert([
            'name'=>'Ernest Hemingway',
            'description'=>''
        ]);
    }
}
