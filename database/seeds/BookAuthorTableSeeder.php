<?php

use Illuminate\Database\Seeder;

class BookAuthorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i<=100;$i++)
        {
            $authorNumber = rand(1,5);
            $authorArr = [];

            for($j = 1; $j<=$authorNumber;$j++)
            {
                $authorArr[] = rand(1, $authorNumber);
            }

            $authorArr = array_unique($authorArr);

            for($j=1;$j<=count($authorArr);$j++)
            {
                DB::table('book_author')->insert([
                    'book_id'=>$i,
                    'author_id'=>$j
                ]);
            }
        }
    }
}
