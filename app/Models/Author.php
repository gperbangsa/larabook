<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public function bookAuthor()
    {
        return $this->hasMany('App\BookAuthor');
    }
}
