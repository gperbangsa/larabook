<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title', 'category_id', 'description', 'image'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function authors()
    {
        return $this->hasManyThrough('App\Models\Author', 'App\Models\BookAuthor', 'book_id', 'id', 'id', 'author_id');
    }
}
