<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookAuthor extends Model
{
    protected $table = 'book_author';
    protected $fillable = ['book_id', 'author_id'];

    public function book()
    {
        return $this->belongsTo('App\Book', 'foreign_key');
    }

    public function author()
    {
        return $this->belongsTo('App\Author', 'foreign_key');
    }
}
