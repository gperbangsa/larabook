<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookRequest;
// use App\Repositories\BookRepository as Book;
// use App\Repositories\BookAuthorRepository as BookAuthor;
use App\Repositories\BookRepositoryInterface as Book;
use App\Repositories\BookAuthorRepositoryInterface as BookAuthor;

class BookController extends Controller
{
    protected $book;
    protected $bookAuthor;

    public function __construct(Book $book, BookAuthor $bookAuthor)
    {
        $this->book = $book;
        $this->bookAuthor = $bookAuthor;
    }

    public function index(Request $request)
    {
        try{
            $filter = $request->all();
            $books = $this->book->filter($filter);

            return response()->json($books, 200);
        }
        catch(\Exception $e){
            return response()->json(['msg'=>'error. ',  'data'=> $e->getMessage()], 200);
        }
    }

    public function show($id)
    {
        try{
            $book = $this->book->find($id);
        
            if(is_null($book))
                return response()->json(["book not found"], 200);

            return response()->json($book);
        }
        catch(\Exception $e){
            return response()->json(['msg'=>'error. ',  'data'=> $e->getMessage()], 200);
        }
    }

    public function store(BookRequest $request)
    {
        try{
            $attributes = [ 'title'=> $request->input('title'),
                            'category_id'=> $request->input('category_id'),
                            'description'=> $request->input('description'),                        
                    ];

            $book = $this->book->create($attributes);

            $attributes['id'] = $book->id;

            if($request->input('author'))
            {
                $authors = $request->input('author');

                foreach($authors as $author)
                {
                    $this->bookAuthor->create(['author_id'=>$author['id'], 'book_id'=>$book->id]);
                }
            }
        
            return response()->json(['msg'=>'Book Created', 
                                     'data'=>['id'=>$attributes]
                                    ], 200);
        }
        catch(\Exception $e){
            return response()->json(['msg'=>'error. ',  'data'=> $e->getMessage()], 200);
        }
    }

    public function update(BookRequest $request, $id)
    {
        try{
            $book = $this->book->find($id);

            if(is_null($book))
                return response()->json(["book not found"], 200);

            $attributes = [ 'title'=>$request->input('title'),
                            'category_id'=>$request->input('category_id'),
                            'description'=>$request->input('description'),
                        ];

            $this->book->update($id, $attributes);
            $attributes['id'] = $id;

            if(!empty($request->input('authors')))
            {
                $this->bookAuthor->deleteByBook($id);

                $authors = $request->input('author');

                foreach($authors as $author)
                {
                    $this->bookAuthor->create(['author_id'=>$author['id'], 'book_id'=>$book->id]);
                }
            }

            return response()->json(['msg'=>'Book Updated',
                                    'data'=>$attributes], 200);
        }
        catch(\Exception $e){
            return response()->json(['msg'=>'error',
                                     'data'=>$e->getMessage()], 200);
        }
    }

    public function destroy($id)
    {
        try{
            $book = $this->book->find($id);

            if(is_null($book))
                return response()->json(["book not found"], 200);

            $this->book->delete($id);            
            return response()->json(['Book Deleted'], 200);
        }
        catch(\Exception $e){
            return response()->json(['msg'=>'error',
                                    'data'=>$e->getMessage()], 200);
        }        
    }

    public function imageUpload(Request $request)
    {
        try{
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,bmp,png,jpg,gif'
            ]);
    
            $bookId       = $request->get('id');
            $uploadedFile = $request->file('image');
            $path         = $uploadedFile->store('public/files');

            return response()->json([
                                'msg'=>'success',
                                'filename'=>$path
                            ]);
        }
        catch(\Exception $e){
            return response()->json([
                                    'msg'=>'error',
                                    'data'=>$e->getMessage()
                               ]);
        }
    }
}
