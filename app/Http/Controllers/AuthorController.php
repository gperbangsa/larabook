<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AuthorRepository as Author;

class AuthorController extends Controller
{
    protected $author;

    public function __construct(Author $author)
    {
        $this->author = $author;
    }

    public function all()
    {
        $authors = $this->author->all();

        return response()->json($authors, 200);
    }
}
