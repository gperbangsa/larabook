<?php

namespace App\Repositories;

interface BookRepositoryInterface
{
    public function filter($keyword, $limit);
    
    public function all();

    public function create($data);

    public function update($id, $data);

    public function delete($id);

    public function destroy($id);

    public function find($id);
}