<?php

namespace App\Repositories;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Repositories\AuthorRepositoryInterface',
            'App\Repositories\AuthorRepository'
        );

        $this->app->bind(
            'App\Repositories\BookAuthorRepositoryInterface',
            'App\Repositories\BookAuthorRepository'
        );

        $this->app->bind(
            'App\Repositories\BookRepositoryInterface',
            'App\Repositories\BookRepository'
        );

        $this->app->bind(
            'App\Repositories\CategoryRepositoryInterface',
            'App\Repositories\CategoryRepository'
        );
    }
}