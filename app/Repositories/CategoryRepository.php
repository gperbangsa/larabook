<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository implements CategoryRepositoryInterface
{    
    public function all()
    {
        return Category::all();
    }

    public function find($id)
    {
        $category = Category::find($id);

        return $category;
    }
}