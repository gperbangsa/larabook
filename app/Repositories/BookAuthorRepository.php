<?php

namespace App\Repositories;

use App\Models\BookAuthor;

class BookAuthorRepository implements BookAuthorRepositoryInterface
{

    public function create($data)
    {
        return BookAuthor::create($data);
    }

    public function update($id, $data)
    {
        
    }

    public function delete($id)
    {
        
    }

    public function deleteByBook($bookId)
    {
        return BookAuthor::where('book_id', $bookId)->delete();
    }

    public function destroy($id)
    {
        
    }

    public function find($id)
    {
        
    }
}