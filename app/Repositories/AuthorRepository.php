<?php

namespace App\Repositories;

use App\Models\Author;

class AuthorRepository implements AuthorRepositoryInterface
{    
    public function all()
    {
        return Author::all();
    }

    public function find($id)
    {
        $author = Author::find($id);

        return $author;
    }
}