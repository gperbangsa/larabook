<?php

namespace App\Repositories;

interface BookAuthorRepositoryInterface
{

    public function create($data);

    public function update($id, $data);

    public function delete($id);

    public function deleteByBook($bookId);

    public function destroy($id);

    public function find($id);
}