<?php

namespace App\Repositories;

use App\Models\Book;

class BookRepository implements BookRepositoryInterface
{
    public function filter($keyword, $limit = 10)
    {

        $book = (new Book)->newQuery();

        if(!empty($keyword['title']))        
            $book->orWhere('title', 'like', '%' . $keyword['title'] . '%');        

        if(!empty($keyword['category_id']))        
            $book->orWhere('category_id',  $keyword['category_id']);        

        if(!empty($keyword['category_name']))
        {
            $book->orWhereHas('category', function ($query) use ($keyword){
                $query->where('categories.name', 'like', '%' . $keyword['category_name']. '%');
            });
        }

        if(!empty($keyword['author_id']))
        {

        }

        if(!empty($keyword['author_name']))
        {
            $book->orWhereHas('authors', function ($query) use ($keyword){
                $query->where('authors.name', 'like', '%' . $keyword['author_name']. '%');
            });
        }

        return $book->with(['category','authors'])->paginate($limit);
    }
    
    public function all()
    {
        
    }

    public function create($data)
    {
        return Book::create($data);
    }

    public function update($id, $data)
    {
        return Book::find($id)->update($data);
    }

    public function delete($id)
    {
        return Book::find($id)->delete();
    }

    public function destroy($id)
    {
        return Book::destroy($id);
    }

    public function find($id)
    {
        $book = Book::find($id);
        $book['category'] = $book->category;
        $book['authors'] = $book->authors;

        return $book;
    }
}