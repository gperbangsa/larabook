<?php

namespace App\Repositories;

interface AuthorRepositoryInterface
{    
    public function all();
    public function find($id);
}