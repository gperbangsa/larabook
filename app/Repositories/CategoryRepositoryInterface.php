<?php

namespace App\Repositories;

interface CategoryRepositoryInterface
{    
    public function all();
    public function find($id);
}